package vn.com.tuanminh.student01;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.naming.directory.InvalidAttributesException;

class StudentTest {
	public static void main(String[] args) {
		int choice = 0;
		Scanner scanner = new Scanner(System.in);

		final String dataPath = "assets/student_list.txt";

		do {
			System.out.printf("Menu\n\n................................\n");
			System.out.println("1. Add a list of student and save to file");
			System.out.println("2. Loading student list from file");
			System.out.println("3. Exit");

			choice = inputInt(scanner, "Your choice: ", 1, 3, "Choice 1-3");

			switch (choice) {
			case 1:
				ArrayList<Student> studentList = inputStudent(scanner);
				writeFile(dataPath, studentList);
				break;
			case 2:
				ArrayList<Student> students = readFile(dataPath);

				for (Student student : students) {
					System.out.printf("%s\n\n", student.toString());
				}
				break;
			case 3:
				break;
			}
		} while (choice > 3 || choice < 1);

		scanner.close();
	}

	private static String inputString(Scanner scanner, String instruction, String[] excluded, String err) {
		boolean isPassed = false;
		String str;

		System.out.print(instruction);
		do {
			str = scanner.nextLine();

			for (int i = 0; i < excluded.length; i++) {
				if (str.contains(excluded[i])) {
					System.err.print(err);
				} else {
					isPassed = true;
				}
			}
		} while (!isPassed);

		// scanner.nextLine();
		return str;
	}

	private static int inputInt(Scanner scanner, String instruction, int min, int max, String err) {
		boolean isPassed = false;
		int value;

		System.out.print(instruction);
		do {
			while (!scanner.hasNextInt()) {
				System.err.print("This is not an integer. Re type: ");
				scanner.next();
			}

			value = scanner.nextInt();

			if (min <= value && value <= max) {
				isPassed = true;
			} else {
				System.err.println(err);
			}
		} while (!isPassed);

		scanner.nextLine();
		return value;
	}

	private static ArrayList<Student> inputStudent(Scanner scanner) {
		int choice = 0;
		String name = null;
		int age = 0;
		int mark = 0;

		ArrayList<Student> students = new ArrayList<>();

		String[] excluded = { " " };
		do {
			name = inputString(scanner, "Enter Name: ", excluded, "Student Name does not containt space. Re type: ");

			age = inputInt(scanner, "Enter age: ", 10, 100, "Age must be from 10 to 100");

			mark = inputInt(scanner, "Enter mark: ", 0, 10, "Mark must be from 0 to 10");

			students.add(new Student(name, age, mark));

			choice = inputInt(scanner, "Write to file now? Yes-1 No-0: ", 0, 1, "Enter 1 for Yes, 0 for No");

		} while (choice == 0);

		return students;
	}

	private static void writeFile(String path, ArrayList<Student> students) {
		try {
			File file = new File(path);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			for (Student student : students) {
				bufferedWriter.write(student.getName() + "\t" + student.getAge() + "\t" + student.getMark() + "\n");
			}

			bufferedWriter.close();
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static ArrayList<Student> readFile(String path) {
		ArrayList<Student> studentList = new ArrayList<>();

		try {
			FileReader fileReader = new FileReader(path);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			String line = null;

			while ((line = bufferedReader.readLine()) != null) {
				Student student = parserDataLine(line);

				studentList.add(student);
			}

			bufferedReader.close();
			fileReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAttributesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
		return studentList;
	}

	private static Student parserDataLine(String dataLine) throws InvalidAttributesException {
		String[] contents = dataLine.split("\t");

		if (contents.length != 3) {
			throw new InvalidAttributesException("Data file contain invalid data format");
		}

		Student student = new Student(contents[0], Integer.parseInt(contents[1]), Integer.parseInt(contents[2]));

		return student;
	}
}
