package vn.com.tuanminh.student02;

import java.io.Serializable;

class Student implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String _name;
	private int _mark;
	private int _age;

	public Student(String name, int age, int mark) {
		_name = name;
		_mark = mark;
		_age = age;
	}

	public String getName() {
		return _name;
	}

	public int getAge() {
		return _age;
	}

	public int getMark() {
		return _mark;
	}

	@Override
	public String toString() {
		return "Name: " + _name + "\nAge: " + _age + "\nMark: " + _mark;
	}
}
