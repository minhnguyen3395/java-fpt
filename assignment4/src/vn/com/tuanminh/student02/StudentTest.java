package vn.com.tuanminh.student02;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

class StudentTest {
	public static void main(String[] args) {
		int choice = 0;
		Scanner scanner = new Scanner(System.in);

		final String dataPath = "assets/student_list.ser";

		do {
			System.out.printf("Menu\n\n................................\n");
			System.out.println("1. Add a list of student and save to file");
			System.out.println("2. Loading student list from file");
			System.out.println("3. Exit");
			System.out.print("Your choice: ");

			choice = scanner.nextInt();
			scanner.nextLine();

			switch (choice) {
			case 1:
				ArrayList<Student> studentList = inputStudent(scanner);

				writeFile(dataPath, studentList);
				break;
			case 2:
				ArrayList<Student> students = readFile(dataPath);

				if (students == null) {
					System.err.println("Parsing data error");
				}

				for (Student student : students) {
					System.out.printf("%s\n\n", student.toString());
				}
				break;
			case 3:
				break;
			default:
				System.err.println("You have to enter an integer from 1 to 3");
				break;
			}
		} while (choice < 3);

		scanner.close();
	}

	private static int inputInt(Scanner scanner, String instruction, int min, int max, String err) {
		boolean isPassed = false;
		int value;

		do {
			System.out.print(instruction);
			value = scanner.nextInt();

			if (min <= value && value <= max) {
				isPassed = true;
			} else {
				System.err.println(err);
			}
		} while (!isPassed);

		scanner.nextLine();
		return value;
	}

	private static ArrayList<Student> inputStudent(Scanner scanner) {
		int choice = 0;
		String name = null;
		int age = 0;
		int mark = 0;

		ArrayList<Student> students = new ArrayList<>();

		do {
			System.out.print("Enter Name: ");
			name = scanner.nextLine();

			age = inputInt(scanner, "Enter age: ", 10, 100, "Age must be from 10 to 100");

			mark = inputInt(scanner, "Enter mark: ", 0, 10, "Mark must be from 0 to 10");

			students.add(new Student(name, age, mark));

			choice = inputInt(scanner, "Write to file now? Yes-1 No-0: ", 0, 1, "Enter 1 for Yes, 0 for No");

		} while (choice == 0);

		return students;
	}

	private static void writeFile(String path, ArrayList<Student> students) {
		try {
			File file = new File(path);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileOutputStream outputStream = new FileOutputStream(file);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

			objectOutputStream.writeObject(students);

			objectOutputStream.close();
			outputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private static ArrayList<Student> readFile(String path) {
		ArrayList<Student> studentList = null;

		try {
			FileInputStream inputStream = new FileInputStream(path);
			ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

			studentList = (ArrayList<Student>) objectInputStream.readObject();

			objectInputStream.close();
			inputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return studentList;
	}
}
