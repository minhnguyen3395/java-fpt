package vn.com.tuanminh.encryption;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

class Encryptor {

	public Encryptor() {
		// TODO Auto-generated constructor stub
	}

	public void encryptFile(String filePath) {
		String content = readFile(filePath);

		System.out.printf("Source data:\n%s", content);

		StringBuilder encryptedContent = new StringBuilder(content);
		for (int i = 0; i < content.length(); i++) {
			char character = content.charAt(i);
			encryptedContent.setCharAt(i, (char) ((int) character + 1));
		}

		System.out.printf("\nEncrypted data:\n%s", encryptedContent);

		writeFile(filePath, encryptedContent.toString());
	}

	private String readFile(String path) {
		String content = null;
		try {
			Scanner scanner = new Scanner(new File(path));
			content = scanner.useDelimiter("\\Z").next();
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return content;
	}

	private void writeFile(String filePath, String content) {
		try {
			FileWriter fileWriter = new FileWriter(filePath);
			fileWriter.write(content);
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
