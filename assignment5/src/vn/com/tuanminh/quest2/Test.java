package vn.com.tuanminh.quest2;

class Test {
	public static void main(String[] args) {
		Thread thread1 = new Thread(new DisplayTask());
		Thread thread2 = new Thread(new DisplayTask());
		Thread thread3 = new Thread(new DisplayTask());

		thread1.start();
		thread2.start();
		thread3.start();
	}
}
