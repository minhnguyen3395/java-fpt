package vn.com.tuanminh.quest2;

class DisplayTask implements Runnable {
	private String[] course = { "Java", "J2EE", "Spring", "Struts" };

	@Override
	public void run() {
		for (int i = 0; i < course.length; i++) {
			System.out.println(course[i]);
		}
	}

}
