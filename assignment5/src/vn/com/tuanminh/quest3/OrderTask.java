package vn.com.tuanminh.quest3;

class OrderTask implements Runnable {
	private Inventory _inventory;
	private int _orderQuantity;

	public OrderTask(Inventory inventory, int quantity) {
		_orderQuantity = quantity;
		_inventory = inventory;
	}

	@Override
	public void run() {
		_inventory.order(_orderQuantity);
	}

}
