package vn.com.tuanminh.quest3;

class InventoryTest {

	public static void main(String[] args) {
		Inventory inventory = new Inventory(500);

		Thread thread1 = new Thread(new OrderTask(inventory, 13));
		thread1.setName("Thread1Thread[test thread,5,main]");

		Thread thread2 = new Thread(new OrderTask(inventory, 91));
		thread2.setName("Thread2Thread[test thread,5,main]");

		System.out.println(thread1.getName());
		System.out.println(thread2.getName());

		thread1.start();
		try {
			thread1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		thread2.start();
	}

}
