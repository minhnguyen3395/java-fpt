package vn.com.tuanminh.quest3;

class Inventory {
	private int _handQuantify;
	private int _orderQuantify;

	public Inventory(int handQuantify) {
		_handQuantify = handQuantify;
	}

	public synchronized void order(int quantity) {
		if (quantity <= 0)
			throw new IllegalArgumentException("Order quantity must be bigger than 0");

		if (quantity > _handQuantify)
			throw new IllegalArgumentException("Order quantity must be smaller than hand quantity");

		_handQuantify -= quantity;
		_orderQuantify += quantity;

		System.out.println("Quantity order: " + quantity);
		System.out.println("Quantity on hand: " + _handQuantify);
		System.out.println("Total quantify taken away by way of order: " + _orderQuantify);
	}
}
