package vn.com.tuanminh.quest4;

import java.util.Vector;

class FactoryTest {
	public static void main(String[] args) {
		Manager manager = new Manager(1000);

		Vector<String> taskList = manager.getTaskList();
		Worker worker1 = new Worker("Worker1", taskList);
		Worker worker2 = new Worker("Worker2", taskList);
		Worker worker3 = new Worker("Worker3", taskList);
		Worker worker4 = new Worker("Worker4", taskList);

		manager.start();
		worker1.start();
		worker2.start();
		worker3.start();
		worker4.start();
	}
}
