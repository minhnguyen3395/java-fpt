package vn.com.tuanminh.quest4;

import java.util.Random;
import java.util.Vector;

class Manager extends Thread {
	private Vector<String> _taskList;
	private Random _random;
	private long _period;

	private String[] _taskNames = { "eating", "running", "sleeping", "walking", "climbing", "jogging", "programming",
			"debugging", "seeking" };

	public Manager(long period) {
		_taskList = new Vector<>();
		_random = new Random();
		_period = period;
	}

	@Override
	public void run() {
		while (true) {
			String task = _taskNames[_random.nextInt(_taskNames.length)];

			synchronized (_taskList) {
				_taskList.add(task);
			}

			System.out.println("Manager adds new task: " + task);
			try {
				sleep(_period);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public Vector<String> getTaskList() {
		return _taskList;
	}
}
