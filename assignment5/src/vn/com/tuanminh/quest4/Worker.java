package vn.com.tuanminh.quest4;

import java.util.Random;
import java.util.Vector;

class Worker extends Thread {
	private Vector<String> _taskList;
	private Random _random;

	public Worker(String name, Vector<String> taskList) {
		_taskList = taskList;
		_random = new Random();
		setName(name);
	}

	@Override
	public void run() {
		while (true) {
			synchronized (_taskList) {
				if (_taskList.isEmpty()) {
					continue;
				}

				String task = _taskList.get(_random.nextInt(_taskList.size()));
				_taskList.remove(task);
				System.out.printf("%s perform task %s\n", getName(), task);
			}
		}
	}
}
