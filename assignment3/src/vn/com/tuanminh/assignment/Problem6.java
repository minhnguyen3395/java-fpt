package vn.com.tuanminh.assignment;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Problem6 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter a string: ");
		String inputStr = scanner.nextLine();

		String[] wordArr = inputStr.split(" ");
		System.out.printf("So tu: %d\n", wordArr.length);
		System.out.printf("So cap trung: %d\n", countDuplicateWord(wordArr));

		// Frequency
		HashMap<String, Integer> resultMap = countFrequency(wordArr);
		Iterator<?> it = resultMap.entrySet().iterator();
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it.next();
			System.out.println(pair.getKey() + " = " + pair.getValue());
			it.remove();
		}

		scanner.close();
	}

	private static int countDuplicateWord(String[] words) {
		int count = 0;
		for (int i = 0; i < words.length - 1; i++) {
			for (int k = i + 1; k < words.length; k++) {
				if (words[i].equals(words[k])) {
					count++;
				}
			}
		}
		return count;
	}

	private static HashMap<String, Integer> countFrequency(String[] words) {
		HashMap<String, Integer> map = new HashMap<>();
		for (String w : words) {
			Integer n = map.get(w);
			n = (n == null) ? 1 : ++n;
			map.put(w, n);
		}
		return map;
	}
}
