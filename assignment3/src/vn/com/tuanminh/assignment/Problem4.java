package vn.com.tuanminh.assignment;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Problem4 {
	private HashMap<String, String> _reminderMap;

	public Problem4() {
		_reminderMap = new HashMap<>();
	}

	public boolean checkNumberExist(String number) {
		return _reminderMap.containsValue(number);
	}

	public boolean checkNameExist(String name) {
		return _reminderMap.containsKey(name);
	}

	public String getNumber(String name) {
		return _reminderMap.get(name);
	}

	public void removeRemind(String name) {
		_reminderMap.remove(name);
	}

	public void displayReminder() {
		Iterator<?> it = _reminderMap.entrySet().iterator();
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it.next();
			System.out.println(pair.getKey() + " = " + pair.getValue());
			it.remove();
		}
	}

	private void parserData() {
		String content = readFile("assets/problem4_data.txt");
		String[] dataRows = content.split("\n");

		for (int i = 0; i < dataRows.length; i++) {
			System.out.println(dataRows[i]);

			String[] dataParts = dataRows[i].split(" ");
			_reminderMap.put(dataParts[0], dataParts[1]);
		}
	}

	private String readFile(String filename) {
		String content = null;
		File file = new File(filename);
		FileReader reader = null;

		try {
			reader = new FileReader(file);
			char[] chars = new char[(int) file.length()];
			reader.read(chars);
			content = new String(chars);
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return content;
	}

	public static void main(String[] args) {
		Problem4 problem4Solver = new Problem4();

		System.out.println("Parsing Data");
		problem4Solver.parserData();
		System.out.println("\nIs 3443 exist in reminder? " + problem4Solver.checkNumberExist("3443"));
		System.out.println("\nIs Jack exist in reminder? " + problem4Solver.checkNameExist("Jack"));
		System.out.println("\nNumber of Tina: " + problem4Solver.getNumber("Tina"));

		System.out.println("\nRemove Joy");
		problem4Solver.removeRemind("Joy");

		System.out.println("\nDisplay reminder");
		problem4Solver.displayReminder();
	}
}
