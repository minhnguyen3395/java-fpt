package vn.com.tuanminh.assignment;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.text.NumberFormatter;

public class Problem1 extends JFrame {

	JFormattedTextField field;
	JPanel panel;
	JButton showResultBut;

	public Problem1() {
		panel = new JPanel(new FlowLayout(SwingConstants.LEADING, 10, 10));
		add(panel);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationByPlatform(true);
		setTitle("Problem1");
		setResizable(true);

		// Text field
		NumberFormat format = NumberFormat.getInstance();
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(0);
		formatter.setMaximum(Integer.MAX_VALUE);
		formatter.setAllowsInvalid(false);
		formatter.setCommitsOnValidEdit(true);
		field = new JFormattedTextField(formatter);
		field.setColumns(10);
		panel.add(field);

		// Show result button
		showResultBut = new JButton("Show result");
		showResultBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int number = (int) field.getValue();
				JOptionPane.showMessageDialog(null,
						"Binh phuong: " + Math.pow(number, 2) + "\n" + "Lap phuong: " + Math.pow(number, 3));
			}
		});
		panel.add(showResultBut);

		pack();
		setVisible(true);
	}

	public static void main(String[] args) {
		Problem1 solver = new Problem1();
	}
}
