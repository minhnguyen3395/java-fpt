package vn.com.tuanminh.assignment;

import java.util.ArrayList;
import java.util.Scanner;

public class Problem3 {

	public static void main(String[] args) {
		ArrayList<Integer> intList = new ArrayList<>();
		Scanner scanner = new Scanner(System.in);

		System.out.println("Please enter list of number:");

		boolean isContinueInput = true;

		do {
			intList.add(scanner.nextInt());
			isContinueInput = scanner.nextBoolean();
		} while (isContinueInput);

		System.out.printf("Binh phuong so lon nhat: %d\n", (int) Math.pow(findMax(intList), 2));
		System.out.printf("Binh phuong so nho nhat: %d\n", (int) Math.pow(findMin(intList), 2));

		scanner.close();
	}

	private static int findMax(ArrayList<Integer> intList) {
		int max = intList.get(0);
		for (int i = 1; i < intList.size(); i++) {
			if (intList.get(i) > intList.get(i - 1))
				max = intList.get(i);
		}
		return max;
	}

	private static int findMin(ArrayList<Integer> intList) {
		int min = intList.get(0);
		for (int i = 1; i < intList.size(); i++) {
			if (intList.get(i) < intList.get(i - 1))
				min = intList.get(i);
		}
		return min;
	}
}
