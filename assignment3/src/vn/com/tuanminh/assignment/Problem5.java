package vn.com.tuanminh.assignment;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Problem5 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		boolean isContinue = true;

		do {
			System.out.println("Enter phone number");
			processPhoneNumber(scanner.nextLine());

			System.out.println("Do you want to continue true/false?");
			isContinue = scanner.nextBoolean();
		} while (isContinue);

		scanner.close();
	}

	private static void processPhoneNumber(String phone) {
		StringTokenizer tokenizer = new StringTokenizer(phone);
		System.out.printf("Ma quoc gia: %s\n", tokenizer.nextToken(" "));
		System.out.printf("Ma vung: (%s)\n", tokenizer.nextToken("-").trim());
		System.out.printf("Ma vung: (%s)\n", tokenizer.nextToken("-"));
	}
}
