package vn.com.tuanminh.networking;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

class Client {
	public static void main(String[] args) {
		Socket socket = null;
		DataInputStream inputStream = null;
		DataOutputStream outputStream = null;
		BufferedReader bufferedReader = null;

		try {
			bufferedReader = new BufferedReader(new InputStreamReader(System.in));

			System.out.print("Enter server ip address (should be 127.0.0.1): ");
			String ip = bufferedReader.readLine();

			socket = new Socket(ip, 3333);

			inputStream = new DataInputStream(socket.getInputStream());
			outputStream = new DataOutputStream(socket.getOutputStream());

			String msgin = "";
			String msgout = "";

			while (true) {
				msgout = bufferedReader.readLine();
				outputStream.writeUTF(msgout);

				if (msgout.equals("bye"))
					break;

				msgin = inputStream.readUTF();
				System.out.print(msgin);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				bufferedReader.close();
				inputStream.close();
				outputStream.close();
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
