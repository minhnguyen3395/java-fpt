package vn.com.tuanminh.networking;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

class Server {
	public static void main(String[] args) {
		ServerSocket serverSocket = null;
		Socket socket = null;
		DataInputStream inputStream = null;
		DataOutputStream outputStream = null;

		try {
			serverSocket = new ServerSocket(3333);
			socket = serverSocket.accept();

			inputStream = new DataInputStream(socket.getInputStream());
			outputStream = new DataOutputStream(socket.getOutputStream());

			String msgin = "";
			String msgout = "";

			while (true) {
				msgin = inputStream.readUTF();

				if (msgin.equals("bye"))
					break;

				System.out.print(msgin);

				msgout = "Hello Client, you have sent to me a message: " + msgin + "\n";
				outputStream.writeUTF(msgout);
				outputStream.flush();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
				outputStream.close();
				socket.close();
				serverSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
